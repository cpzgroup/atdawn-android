# 如何使用GIT #

- 打开命令行或终端
- cd 到你的目录
- git pull origin next:master 取回origin主机的next分支，与本地的master分支合并
- git status 查看当前你做的未被提交的修改
- git add --all 将所有修改提交到本地
- git commit -m "说明" 将此次修改命名
- git push origin local:remote 将本地local分支提交到远程的remote分支

# 获取资源文件 #

我们所有的资源文件，包括设计图等资料，请访问链接: http://pan.baidu.com/s/1i37ty1v 密码: nsgq 查看和下载

# 查看API #

要想和服务器交互，需要按照规定访问API，本项目WIKI包含了API访问方法。请点击左边倒数第三个ICON。

# 其他资料 #

安卓端搭建即时聊天系统：https://leancloud.cn/docs/android_realtime_v2.html

安卓登陆后的activity管理： https://github.com/greenrobot/EventBus

# 分工 #

陈心远 ：会话列表页、即时聊天页、朋友圈页面

钟  典 ：推送、后端

潘星阳 ：其他